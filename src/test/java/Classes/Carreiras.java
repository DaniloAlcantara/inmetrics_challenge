package Classes;

import Infraestrutura.CreateDriver;
import Infraestrutura.Generator;
import Infraestrutura.Screenshot;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;
import org.openqa.selenium.By;

import static org.junit.Assert.assertEquals;

public class Carreiras extends CreateDriver {

    /*@Rule
    public TestName teste = new TestName();
*/
    @Dado("^que acesso o site carreiras da inmetrics$")
    public void queacessoositecarreirasdainmetrics(){
        CreateDriver.createChrome();
        System.out.print(" step 1 passou");
    }

    @E("^Navego ate Carreiras$")
    public void NavegoateCarreiras(){
        driver.findElement(By.cssSelector("#linkcarreiras")).click();
        System.out.print(" step 2 passou");
    }

    @Quando("^clicar no botão confira nossas vagas$")
    public void clicarnobotãoconfiranossasvagas(){

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        driver.findElement(By.xpath("//a[contains(text(), 'confira nossas vagas')]")).click(); //.col-md-6 > .btn
        System.out.print(" step 3 passou");
    }

    @Entao("^mensagem e exibida$")
    public void mensagemeexibida(){
        assertEquals("12 MOTIVOS PARA TRABALHAR CONOSCO", driver.findElement(By.cssSelector("#section-jobs")).getText());
        System.out.print(" step 4 passou");
        String screenshotArquivo = "C:/Inmetrics_Challenge/target/web/" + Generator.dataHoraParaArquivo()+ /*teste.getMethodName()*/  ".png";
        Screenshot.tirar(driver, screenshotArquivo );
        driver.quit();

    }

}
