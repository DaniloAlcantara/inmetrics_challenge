package Infraestrutura;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.IOException;


public class Screenshot {
    public static void tirar(WebDriver navegador, String Arquivo){
        File screenshot = ((TakesScreenshot)navegador).getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(screenshot, new File(Arquivo));
        } catch (IOException e) {
            System.out.println("Problemas ao copiar arquivo" + e.getMessage());
        }
    }
}
